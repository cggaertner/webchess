<?php
# Copyright 2014 Christoph Gärtner
# Distributed under the GNU Affero General Public License, Version 3 or later

define('MAX_FILESIZE', 1 << 12);
define('REALM', 'WebChess');
define('CONTENT_TYPE', 'Content-Type: text/html; charset=utf-8');
define('DATA_DIR', isset($_ENV['OPENSHIFT_DATA_DIR'])
    ? $_ENV['OPENSHIFT_DATA_DIR'] : 'data');

function auth($realm) {
    header('HTTP/1.0 401 Unauthorized');
    header("WWW-Authenticate: Basic realm=\"$realm\"");
}

function auth_user() {
    return isset($_SERVER['PHP_AUTH_USER'])
        ? htmlspecialchars(trim($_SERVER['PHP_AUTH_USER'])) : '';
}

function reauth_on_request() {
    global $player, $url;
    if(!isset($_POST['player']))
        return false;

    if($_POST['player'] === $player)
        auth(REALM);

    die("<script type=\"text/javascript\">location.href = '$url'</script>");
}

function store_move_on_request() {
    global $player, $query;
    if(!isset($_POST['move']))
        return false;

    $move = $_POST['move'];
    if(!preg_match('/^[a-h][1-8]([a-h][1-8]|=[NBRQnbrq])$/', $move))
        sorry();

    $file = DATA_DIR."/$query.$move";
    if(@filesize($file) > MAX_FILESIZE)
        unlink($file);

    file_put_contents($file, "$player\n", FILE_APPEND | LOCK_EX);

    header('HTTP/1.0 204 No Content');
    die;
}

function sorry() {
    header('HTTP/1.0 400 Bad Request');
    die('Sorry, but something\'s wrong with you request.');
}

function player() {
    global $player;
    echo $player;
}

function sort_moves($a, $b) {
    $cmp = ($a[0] < $b[0]) - ($a[0] > $b[0]);
    return $cmp !== 0 ? $cmp : strcmp($a[1], $b[1]);
}

function dump_moves() {
    global $query;
    if(!$query) return;

    $files = glob(DATA_DIR."/$query.*");
    $items = array();
    foreach($files as $file) {
        $move = explode('.', $file)[1];
        $players = file($file,  FILE_IGNORE_NEW_LINES);
        $count = count($players);
        if($count > 10)
            array_splice($players, 5, -5, '&hellip;');

        $items[] = array($count, $move, implode(', ', $players));
    }

    usort($items, 'sort_moves');
    if(count($items) > 0) {
        foreach($items as $item) {
            printf('<div><span>%1$03u</span> '
                .'<a id="move-%2$s"'
                .' href="javascript:WebChess.browse(\'%2$s\')">%2$s</a>'
                .'<br>by <i>%3$s</i></div>'."\n", $item[0], $item[1], $item[2]);
        }
    }
}

header(CONTENT_TYPE);

$url   = $_SERVER['REQUEST_URI'];
$query = $_SERVER['QUERY_STRING'];
if(!preg_match('/^[\w-]*$/', $query))
    sorry();

$player = 'guest';
$user   = auth_user();
if($user !== '')
    $player = $user;

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    reauth_on_request() || store_move_on_request();
    sorry();
}

require 'template.html';
?>
