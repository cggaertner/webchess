/*
    Copyright (C) 2014  Christoph Gärtner

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

(function() {
    WebChess = (function() {
        return {
            pasteAs: pasteAs,
            restart: restart,
            refresh: refresh,
            back: back,
            forward: forward,
            browse: browse,
            clear: clear,
            switchTurn: switchTurn,
            kill: kill,
            spawn: spawn,
            setEnPassant: setEnPassant,
            setCastlingState: setCastlingState,
        };

        function pasteAs(id) {
            table.id = id;
            var node = document.getElementById(id);
            node.parentNode.replaceChild(table, node);
        }

        function restart() { location.search = BASE }
        function refresh() { location.reload() }
        function forward() { history.forward() }
        function back() { history.back() }
        function clear() { location.search = '64_-' + state.activeColor + '--' }

        function switchTurn() {
            state.dup({ activeColor: OTHER_COLOR[state.activeColor] }).load();
        }

        function kill() {
            if(defined(state.selectedPos)
                    && !state.board.isEmpty(state.selectedPos))
                state.dup().setSelected('_').load();
        }

        function spawn(piece) {
            if(!defined(state.selectedPos))
                return;

            var piece = PIECES[piece];
            var promoted = false;
            switch(state.board[state.selectedPos]) {
                case 'P':
                if(state.activeColor !== 'w'
                        || !/[NBRQ]/.test(piece)
                        || (state.selectedPos / 8 | 0) !== 0)
                    return;
                promoted = true;
                post('move=' + fromPos(state.selectedPos) + '=' + piece);
                break;

                case 'p':
                if(state.activeColor !== 'b'
                        || !/[nbrq]/.test(piece)
                        || (state.selectedPos / 8 | 0) !== 7)
                    return;
                promoted = true;
                post('move=' + fromPos(state.selectedPos) + '=' + piece);
                break;

                case '_':
                break;

                default:
                return;
            }

            var color = promoted
                ? OTHER_COLOR[state.activeColor]
                : state.activeColor;

            state.dup({ activeColor: color }).setSelected(piece).load();
        }

        function setEnPassant() {
            state.dup({ enPassantPos: state.selectedPos }).load();
        }

        function setCastlingState() {
            var input = prompt('Castling state:', 'KQkq');
            if(!defined(input))
                return;

            var castlingState = {};
            var keys = input.split('');
            for(var i = 0; i < keys.length; ++i)
                castlingState[keys[i]] = true;

            state.dup({ castlingState: castlingState }).load();
        }

        function browse(move) {
            var src  = toPos(move.substr(0, 2));
            if(move.charAt(2) === '=') {
                var piece = move.charAt(3);
                state.dup().promote(src, piece).load();
            }
            else {
                var dest = toPos(move.substr(2, 2));
                state.dup().move(src, dest).load();
            }
        }
    })();

    var UNDEF = void 0;
    var BASE = 'rnbqkbnr8p32_8PRNBQKBNR-w-KQkq-';
    var PIECES = WebChess_PIECES;
    var SAME_COLOR  = { w: 'w', b: 'b' };
    var OTHER_COLOR = { w: 'b', b: 'w' };

    var THREAT_CONSTRAINTS = {
        K: TC_WK,
        P: TC_WP,
        k: TC_BK,
        p: TC_BP,
    };

    function TC_WK(src, dest) { return Math.abs(dest - src) !== 2 }
    function TC_BK(src, dest) { return Math.abs(dest - src) !== 2 }
    function TC_WP(src, dest) { return src % 8 !== dest % 8 }
    function TC_BP(src, dest) { return src % 8 !== dest % 8 }

    var MOVE_CONSTRAINTS = {
        K: MC_WK,
        P: MC_WP,
        k: MC_BK,
        p: MC_BP,
    };

    function MC_WK(src, dest) {
        switch(dest - src) {
            case +2:
            return this.castlingState.K
                && !this.threat[src] && !this.threat[src + 1];

            case -2:
            return this.castlingState.Q
                && !this.threat[src] && !this.threat[src - 1];
        }

        return true;
    }

    function MC_BK(src, dest) {
        switch(dest - src) {
            case +2:
            return this.castlingState.k
                && !this.threat[src] && !this.threat[src + 1];

            case -2:
            return this.castlingState.q
                && !this.threat[src] && !this.threat[src - 1];
        }

        return true;
    }

    function MC_WP(src, dest) {
        switch(dest - src) {
            case -8:
            return this.board.isEmpty(dest);

            case -16:
            return this.board.isEmpty(dest) && (src / 8 | 0) === 6;

            case -7:
            case -9:
            return this.board.colorAt(dest) === OTHER_COLOR[this.activeColor]
                || dest === this.enPassantPos;
        }

        throw 'illegal move for white pawn: ' + (dest - src);
    }

    function MC_BP(src, dest) {
        switch(dest - src) {
            case +8:
            return this.board.isEmpty(dest);

            case +16:
            return this.board.isEmpty(dest) && (src / 8 | 0) === 1;

            case +7:
            case +9:
            return this.board.colorAt(dest) === OTHER_COLOR[this.activeColor]
                || dest === this.enPassantPos;
        }

        throw 'illegal move for black pawn: ' + (dest - src);
    }


    var Board = (function() {
        return {
            bless: bless,
            dup: dup,
            colorAt: colorAt,
            isEmpty: isEmpty,
            isPawn: isPawn,
            isKing: isKing,
        };

        function dup() { return Board.bless(this.slice()) }
        function colorAt(pos) { return PIECES[this[pos]][0] }
        function isEmpty(pos) { return this[pos] === '_' }
        function isPawn(pos)  { return this[pos] === 'P' || this[pos] === 'p' }
        function isKing(pos)  { return this[pos] === 'K' || this[pos] === 'k' }
    })();

    var State = (function() {
        return {
            bless: bless,
            dup: dup,
            load: load,
            render: render,
            analyze: analyze,
            isEnemy: isEnemy,
            isFriend: isFriend,
            isSelected: isSelected,
            canMove: canMove,
            move: move,
            promote: promote,
            inCheck: inCheck,
            direction: direction,
            unselect: unselect,
            isTarget: isTarget,
            setSelected: setSelected,
        };

        function setSelected(piece) {
            this.board[this.selectedPos] = piece;
            return this;
        }

        function dup(override) {
            return mixin({}, this, {
                castlingState: mixin({}, this.castlingState),
                board: this.board.dup(),
            }, override);
        }

        function load() { location.search = serialize(this) }

        function unselect() { this.selectedPos = UNDEF }

        function isTarget(pos) {
            var targets = this.moves[this.selectedPos];
            for(var i = 0; i < targets.length; ++i) {
                if(targets[i] === pos)
                    return true;
            }

            return false;
        }

        function render(table) {
            for(var i = 0; i < 64; ++i) {
                var cell = table.getElementsByTagName('tr')[i / 8 | 0]
                    .getElementsByTagName('td')[i % 8];

                cell.innerHTML = PIECES[this.board[i]][1];

                if(this !== state) {
                    cell.className = 'square';
                    continue;
                }

                cell.className = defined(this.selectedPos)
                    ? (this.isSelected(i) ?
                        'selected square' : 'unselected square')
                    : (this.canMove(i) ?
                        'mobile square' : 'square');
            }

            if(this !== state)
                return;

            var mobile = defined(this.selectedPos)
                && this.board.colorAt(this.selectedPos) === this.activeColor;
            var moves = mobile ? this.moves[this.selectedPos] : [];
            for(var i = 0; i < moves.length; ++i) {
                var cell = document.getElementById(fromPos(moves[i]));
                cell.className = 'target square';
            }
        }

        function isSelected(pos) { return this.selectedPos === pos }

        function canMove(pos) {
            return defined(this.moves[pos]) && this.moves[pos].length > 0;
        }

        function computeReach(piece, src, board) {
            if(piece === '_')
                return [];

            var piece = PIECES[piece];
            var reach = [];
            var sameColor = board.colorAt(src);
            var otherColor = OTHER_COLOR[sameColor];
            var r0 = src / 8 | 0;
            var c0 = src % 8;

        nextDirection:
            for(var i = 2; i < piece.length; ++i) {
                var chain = piece[i];
                for(var j = 0; j < chain.length; j += 2) {
                    var dr = chain[j + 0];
                    var dc = chain[j + 1]
                    var r = r0 + dr;
                    var c = c0 + dc;
                    if(r < 0 || c < 0 || r >= 8 || c >= 8)
                        continue nextDirection;

                    var dest = r * 8 + c;
                    switch(board.colorAt(dest)) {
                        case null:
                        reach.push(dest);
                        break;

                        case otherColor:
                        reach.push(dest);
                        // fallthrough

                        case sameColor:
                        continue nextDirection;
                    }
                }
            }

            return reach;
        }

        function isEnemy(pos) {
            return this.board.colorAt(pos) === OTHER_COLOR[this.activeColor];
        }

        function isFriend(pos) {
            return this.board.colorAt(pos) === this.activeColor;
        }

        function analyze(findMoves) {
            var reach = this.board.map(computeReach, this);
            this.threat = [];

            for(var src = 0; src < 64; ++src) {
                if(!this.isEnemy(src))
                    continue;

                var constraint = threatConstraint(src, this.board[src]);
                var newThreat = constraint
                    ? reach[src].filter(constraint, this)
                    : reach[src];

                for(var i = 0; i < newThreat.length; ++i)
                    this.threat[newThreat[i]] = true;
            }

            if(findMoves) {
                var promo = [];

                if(this.activeColor === 'w') {
                    for(var i = 0; i < 8; ++i) {
                        if(this.board[i] === 'P')
                            promo.push(i);
                    }
                }
                else {
                    for(var i = 56; i < 64; ++i) {
                        if(this.board[i] === 'p')
                            promo.push(i);
                    }
                }

                if(promo.length) {
                    this.moves = {};

                    for(var i = 0; i < promo.length; ++i)
                        this.moves[promo[i]] = [ promo[i] ];
                }
                else this.moves = reach.map(constrainMoves, this);
            }

            return this;
        }

        function constrainMoves(reach, src) {
            if(this.board.colorAt(src) !== this.activeColor)
                return [];

            var constraint = moveConstraint(src, this.board[src]);
            return (constraint ? reach.filter(constraint, this) : reach).filter(
                function(dest) {
                    var clone = this.dup().move(src, dest);
                    clone.activeColor = OTHER_COLOR[clone.activeColor];
                    clone.analyze(false);
                    return !clone.inCheck(this.activeColor);
                }, this);
        }

        function inCheck(color) {
            for(var i = 0; i < 64; ++i) {
                if(this.threat[i]
                        && this.board[i] === (color === 'w' ? 'K' : 'k'))
                    return true;
            }

            return false;
        }

        function direction() { return this.activeColor === 'w' ? -1 : +1 }

        function move(src, dest) {
            var isPawn = this.board.isPawn(src);
            var destRow = (dest / 8 | 0);

            if(isPawn) {
                if(dest === this.enPassantPos)
                    this.board[dest - 8 * this.direction()] = '_';

                this.enPassantPos = Math.abs(dest - src) === 16
                    ? (dest + src) / 2 : UNDEF;
            }
            else {
                this.enPassantPos = UNDEF;

                if(this.board.isKing(src) && Math.abs(dest - src) === 2) {
                    var rookSrc = dest + (src < dest ? +1 : -2);
                    var rookDest = (src + dest) / 2;
                    this.board[rookDest] = this.board[rookSrc];
                    this.board[rookSrc] = '_';
                }

                if(this.activeColor === 'w') {
                    if(src === 60 || src === 63)
                        delete this.castlingState.K;
                    if(src === 60 || src === 56)
                        delete this.castlingState.Q;
                    if(dest === 7)
                        delete this.castlingState.k;
                    if(dest === 0)
                        delete this.castlingState.q;
                }
                else {
                    if(src === 4 || src === 7)
                        delete this.castlingState.k;
                    if(src === 4 || src === 0)
                        delete this.castlingState.q;
                    if(dest === 63)
                        delete this.castlingState.K;
                    if(dest === 56)
                        delete this.castlingState.Q;
                }
            }

            this.board[dest] = this.board[src];
            this.board[src ] = '_';
            if(isPawn && (destRow === 0 || destRow === 7));
            else this.activeColor = OTHER_COLOR[this.activeColor];
            return this;
        }

        function promote(pos, piece) {
            this.board[pos] = piece;
            this.activeColor = OTHER_COLOR[this.activeColor];
            return this;
        }
    })();

    try { var state = State.bless(deserialize(location.search.substr(1))) }
    catch(error) {
        if(defined(error.DONT_PANIC))
            WebChess.restart();
        else throw error;
        return;
    }

    var table = createTable();
    state.analyze(true);
    state.render(table);

    onclick = function(event) {
        if(!/(^|\s)square(\s|$)/.test(event.target.className))
            return;

        var pos = toPos(event.target.id);
        if(defined(state.selectedPos)) {
            if(state.isSelected(pos))
                state.unselect();
            else if(state.isTarget(pos) && state.activeColor
                    === state.board.colorAt(state.selectedPos)) {
                var src = state.selectedPos;
                var dest = pos;
                state.unselect();
                post('move=' + fromPos(src) + fromPos(dest));
                state.dup().move(src, dest).load();
            }
        }
        else state.selectedPos = pos;

        state.render(table);
    }

    onmouseover = function(event) {
        if(!/^move-/.test(event.target.id))
            return;

        var move = event.target.id.substr(5);
        var src  = toPos(move.substr(0, 2));
        if(move.charAt(2) === '=') {
            var piece = move.charAt(3);
            state.dup().promote(src, piece).render(table);
        }
        else {
            var dest = toPos(move.substr(2, 2));
            state.dup().move(src, dest).render(table);
        }
    };

    onmouseout = function(event) {
        if(/^move-/.test(event.target.id))
            state.render(table);
    };

    function defined(value) { return value !== UNDEF && value !== null }

    function panic(msg) {
        var error = new Error(msg);
        error.DONT_PANIC = 42;
        return error;
    }

    function bless(obj) { return mixin(obj, this) }

    function mixin(dest/*, src...*/) {
        for(var i = 1; i < arguments.length; ++i) {
            var src = arguments[i];
            for(var key in src) {
                if(src.hasOwnProperty(key))
                    dest[key] = src[key];
            }
        }

        return dest;
    }

    function post(msg) {
        var req = new XMLHttpRequest;
        req.open('POST', location.href, false);
        req.setRequestHeader('Content-type',
            'application/x-www-form-urlencoded');
        req.send(msg);
    }

    function toPos(str) {
        return /^[a-h][1-8]/.test(str)
            ? str.charCodeAt(0) - 'a'.charCodeAt(0)
                + 8 * (8 + '0'.charCodeAt(0) - str.charCodeAt(1))
            : UNDEF;
    }

    function fromPos(pos) {
        return 0 <= pos && pos < 64
            ? String.fromCharCode(
                'a'.charCodeAt(0) + (pos % 8),
                '8'.charCodeAt(0) - (pos / 8 | 0))
            : UNDEF;
    }

    function threatConstraint(src, piece) {
        var constraint = THREAT_CONSTRAINTS[piece];
        return constraint
            ? function(dest) { return constraint.call(this, src, dest) }
            : UNDEF;
    }

    function moveConstraint(src, piece) {
        var constraint = MOVE_CONSTRAINTS[piece];
        return constraint
            ? function(dest) { return constraint.call(this, src, dest) }
            : UNDEF;
    }

    function serialize(obj) {
        return [
            obj.board.join('').replace(/(\w)\1+/g, function(match, p1) {
                return String(match.length) + p1;
            }),
            obj.activeColor,
            Object.keys(obj.castlingState).sort().join(''),
            (obj.enPassantPos && fromPos(obj.enPassantPos)) || '',
        ].join('-');
    }

    function deserialize(str) {
        var tokens = str.split('-');
        if(tokens.length !== 4)
            throw panic('trying to deserialize illegal state string');

        var board = (tokens[0].match(/(\d*\w)/g) || []).map(function(match) {
            var count = parseInt(match, 10);
            var piece;
            if(isNaN(count)) {
                count = 1;
                piece = match;
            }
            else piece = match.substr(String(count).length);
            return new Array(count + 1).join(piece);
        }).join('').split('');
        if(board.length !== 64)
            throw panic('board deserialization failed');

        var activeColor = SAME_COLOR[tokens[1]];
        if(!defined(activeColor))
            throw panic('active color deserialization failed');

        var enPassantPos = defined(tokens[3]) && toPos(tokens[3]);
        var castlingState = {};
        var keys = tokens[2] ? tokens[2].split('') : [];
        for(var i = 0; i < keys.length; ++i)
            castlingState[keys[i]] = true;

        return State.bless({
            board: Board.bless(board),
            activeColor: activeColor,
            enPassantPos: enPassantPos,
            castlingState: castlingState,
            selectedPos: UNDEF,
        });
    }

    function createTable() {
        var cell = document.createElement('td');

        var row = document.createElement('tr');
        for(var i = 0; i < 8; ++i)
            row.appendChild(cell.cloneNode(true));
        row.appendChild(document.createElement('th'));

        var table = document.createElement('table');
        for(var i = 0; i < 8; ++i) {
            var clone = table.appendChild(row.cloneNode(true));
            clone.lastChild.appendChild(document.createTextNode(8 - i));
        }

        for(var i = 0; i < 64; ++i) {
            var cell = table.childNodes[i / 8 | 0].childNodes[i % 8];
            cell.title = cell.id = fromPos(i);
        }

        var labels = 'abcdefgh';
        var row = table.appendChild(document.createElement('tr'));
        for(var i = 0; i < 8; ++i) {
            row.appendChild(
                document.createElement('th').appendChild(
                    document.createTextNode(labels.charAt(i))).parentNode);
        }

        return table;
    }
})();
